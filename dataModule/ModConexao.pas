unit ModConexao;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.SqlExpr, Data.DBXMySQL,
  Data.FMTBcd, Datasnap.DBClient, Datasnap.Provider;

type
  TDM = class(TDataModule)
    SQLConection: TSQLConnection;
    SdsCaixa: TSQLDataSet;
    dspCaixa: TDataSetProvider;
    cdsCaixa: TClientDataSet;
    SdsContasPagar: TSQLDataSet;
    dspContasReceber: TDataSetProvider;
    cdsContasPagar: TClientDataSet;
    SdsContasReceber: TSQLDataSet;
    dspContasPagar: TDataSetProvider;
    cdsContasReceber: TClientDataSet;
    SdsUsuarios: TSQLDataSet;
    dspUsuario: TDataSetProvider;
    cdsUsuario: TClientDataSet;
    cdsCaixaid: TIntegerField;
    cdsCaixanumero_doc: TStringField;
    cdsCaixadescricao: TStringField;
    cdsCaixavalor: TFMTBCDField;
    cdsCaixatipo: TStringField;
    cdsCaixadt_cadastro: TDateField;
    cdsUsuarioid: TIntegerField;
    cdsUsuarionome: TStringField;
    cdsUsuariologin: TStringField;
    cdsUsuariosenha: TStringField;
    cdsUsuariostatus: TStringField;
    cdsUsuariodt_cadastro: TDateField;
    cdsContasReceberid: TIntegerField;
    cdsContasRecebernumero_doc: TStringField;
    cdsContasReceberdescricao: TStringField;
    cdsContasRecebervalor: TFMTBCDField;
    cdsContasRecebertipo: TStringField;
    cdsContasReceberdt_cadastro: TDateField;
    cdsContasPagarid: TIntegerField;
    cdsContasPagarnumero_doc: TStringField;
    cdsContasPagardescricao: TStringField;
    cdsContasPagarparcela: TIntegerField;
    cdsContasPagarvlr_parcela: TFMTBCDField;
    cdsContasPagarvlr_compra: TFMTBCDField;
    cdsContasPagarvlr_abatido: TFMTBCDField;
    cdsContasPagardt_compra: TDateField;
    cdsContasPagardt_cadastro: TDateField;
    cdsContasPagardt_vencimento: TDateField;
    cdsContasPagardt_pagamento: TDateField;
    cdsContasPagarstatus: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
