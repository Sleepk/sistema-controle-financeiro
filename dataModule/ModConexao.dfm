object DM: TDM
  OldCreateOrder = False
  Height = 274
  Width = 437
  object SQLConection: TSQLConnection
    ConnectionName = 'financeiro'
    DriverName = 'MySQL'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=MySQL'
      'DriverUnit=Data.DBXMySQL'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver220.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=22.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXMySqlMetaDataCommandFactory,DbxMySQLDr' +
        'iver220.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXMySqlMetaDataCommandFact' +
        'ory,Borland.Data.DbxMySQLDriver,Version=22.0.0.0,Culture=neutral' +
        ',PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxmys.dll'
      'LibraryNameOsx=libsqlmys.dylib'
      'VendorLib=LIBMYSQL.dll'
      'VendorLibWin64=libmysql.dll'
      'VendorLibOsx=libmysqlclient.dylib'
      'HostName=localhost'
      'Database=financeiro'
      'User_Name=root'
      'Password=p@ssword'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'Compressed=False'
      'Encrypted=False'
      'BlobSize=-1'
      'ErrorResourceFile=')
    Connected = True
    Left = 48
    Top = 24
  end
  object SdsCaixa: TSQLDataSet
    CommandText = 'select * from `caixa`'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConection
    Left = 120
    Top = 40
  end
  object dspCaixa: TDataSetProvider
    DataSet = SdsCaixa
    Left = 200
    Top = 40
  end
  object cdsCaixa: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCaixa'
    Left = 328
    Top = 40
    object cdsCaixaid: TIntegerField
      FieldName = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsCaixanumero_doc: TStringField
      FieldName = 'numero_doc'
    end
    object cdsCaixadescricao: TStringField
      FieldName = 'descricao'
      Size = 200
    end
    object cdsCaixavalor: TFMTBCDField
      FieldName = 'valor'
      Precision = 19
      Size = 0
    end
    object cdsCaixatipo: TStringField
      FieldName = 'tipo'
      FixedChar = True
      Size = 1
    end
    object cdsCaixadt_cadastro: TDateField
      FieldName = 'dt_cadastro'
    end
  end
  object SdsContasPagar: TSQLDataSet
    CommandText = 'select * from `contas_pagar`'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConection
    Left = 112
    Top = 88
  end
  object dspContasReceber: TDataSetProvider
    DataSet = SdsContasReceber
    Left = 184
    Top = 144
  end
  object cdsContasPagar: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspContasPagar'
    Left = 328
    Top = 88
    object cdsContasPagarid: TIntegerField
      FieldName = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsContasPagarnumero_doc: TStringField
      FieldName = 'numero_doc'
    end
    object cdsContasPagardescricao: TStringField
      FieldName = 'descricao'
      Size = 200
    end
    object cdsContasPagarparcela: TIntegerField
      FieldName = 'parcela'
    end
    object cdsContasPagarvlr_parcela: TFMTBCDField
      FieldName = 'vlr_parcela'
      Precision = 20
      Size = 2
    end
    object cdsContasPagarvlr_compra: TFMTBCDField
      FieldName = 'vlr_compra'
      Precision = 20
      Size = 2
    end
    object cdsContasPagarvlr_abatido: TFMTBCDField
      FieldName = 'vlr_abatido'
      Precision = 20
      Size = 2
    end
    object cdsContasPagardt_compra: TDateField
      FieldName = 'dt_compra'
    end
    object cdsContasPagardt_cadastro: TDateField
      FieldName = 'dt_cadastro'
    end
    object cdsContasPagardt_vencimento: TDateField
      FieldName = 'dt_vencimento'
    end
    object cdsContasPagardt_pagamento: TDateField
      FieldName = 'dt_pagamento'
    end
    object cdsContasPagarstatus: TStringField
      FieldName = 'status'
      FixedChar = True
      Size = 1
    end
  end
  object SdsContasReceber: TSQLDataSet
    CommandText = 'select * from `caixa`'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConection
    Left = 112
    Top = 136
  end
  object dspContasPagar: TDataSetProvider
    DataSet = SdsContasPagar
    Left = 184
    Top = 96
  end
  object cdsContasReceber: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspContasReceber'
    Left = 328
    Top = 144
    object cdsContasReceberid: TIntegerField
      FieldName = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsContasRecebernumero_doc: TStringField
      FieldName = 'numero_doc'
    end
    object cdsContasReceberdescricao: TStringField
      FieldName = 'descricao'
      Size = 200
    end
    object cdsContasRecebervalor: TFMTBCDField
      FieldName = 'valor'
      Precision = 19
      Size = 0
    end
    object cdsContasRecebertipo: TStringField
      FieldName = 'tipo'
      FixedChar = True
      Size = 1
    end
    object cdsContasReceberdt_cadastro: TDateField
      FieldName = 'dt_cadastro'
    end
  end
  object SdsUsuarios: TSQLDataSet
    CommandText = 'select * from `usuarios`'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConection
    Left = 112
    Top = 192
  end
  object dspUsuario: TDataSetProvider
    DataSet = SdsUsuarios
    Left = 176
    Top = 192
  end
  object cdsUsuario: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspUsuario'
    Left = 328
    Top = 200
    object cdsUsuarioid: TIntegerField
      FieldName = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsUsuarionome: TStringField
      FieldName = 'nome'
      Size = 50
    end
    object cdsUsuariologin: TStringField
      FieldName = 'login'
    end
    object cdsUsuariosenha: TStringField
      FieldName = 'senha'
    end
    object cdsUsuariostatus: TStringField
      FieldName = 'status'
      FixedChar = True
      Size = 1
    end
    object cdsUsuariodt_cadastro: TDateField
      FieldName = 'dt_cadastro'
    end
  end
end
