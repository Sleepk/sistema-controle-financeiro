unit Uprincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Imaging.pngimage, Vcl.ComCtrls, Vcl.Buttons, AdvOfficeComboBox,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Intf, FireDAC.Comp.Client;

type
  Tfrmprincipal = class(TForm)
    imgusuarios: TImage;
    imgreceber: TImage;
    imgpagar: TImage;
    imgcaixa: TImage;
    imgconreceber: TImage;
    imgconpagar: TImage;
    Label4: TLabel;
    imgrelpagar: TImage;
    imgconfig: TImage;
    imgnavegador: TImage;
    imgrelreceber: TImage;
    imgrelcaixa: TImage;
    BalloonHint1: TBalloonHint;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmprincipal: Tfrmprincipal;

implementation

uses
  uFrmCadastroBasico;

{$R *.dfm}

/// <sumary> teste arquivos
procedure Tfrmprincipal.BitBtn1Click(Sender: TObject);
begin
  frmCadastroBasico := TfrmCadastroBasico.Create(nil);
  try
    frmCadastroBasico.ShowModal;
  finally
    FreeAndNil(frmCadastroBasico);
  end;
end;

procedure Tfrmprincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Application.MessageBox('Deseja Realmente Sair',
    'Sistema Controle Financeiro', MB_YESNO + MB_ICONQUESTION) = mrYes then
    Application.Terminate
  else
    Abort;

end;

procedure Tfrmprincipal.Timer1Timer(Sender: TObject);
begin
  StatusBar1.Panels.Items[0].Text := DateTimeToStr(now);
end;

end.
